//
//  main.cpp
//  ficha2
//
//  Created by Ana Dulce Vale on 29/10/2020.
//

//
//  main.cpp
//  Triangulo
//
//  Created by Ana Dulce Vale on 09/10/2020.
//


#define GL_SILENCE_DEPRECATION
#include "Triangulo.h"
#include "sistema.h"
#include "Quadrado.hpp"


//Triangulo* oTriangulo = new Triangulo(-17,8, -14,18, -11,8); //criaçao de um objeto oTriangulo
//Triangulo* oTriangulo2 = new Triangulo(-8,-16, -9,-3, 8,-2);
Quadrado* oQuadrado3 = new Quadrado(-10, 14, -10, 17, -8,17, -8,14, 1, 0.3, 0.1);
Triangulo* oTriangulo3 = new Triangulo(-16,14, -12,17, -8,14);

Quadrado* oQuadrado = new Quadrado(-16,14,-8,14,-8,6,-16,6,1.0,1.0,0.9);
Quadrado* oQuadrado1 = new Quadrado(-15, 10, -15, 12, -13, 12, -13, 10,0.7,1,0.7);
Quadrado* oQuadrado2 = new Quadrado(-12, 6, -12, 11, -10, 11, -10, 6, 0.3, 0.1, 0.1);




//Triangulo(1,0,0) pois agora temos 3 argumentos


void teclado(unsigned char tecla, int x, int y)
{
    if (tecla == 27)
        exit(0);
}

void desenho(void)
{
    glClearColor(0, 0, 0, 0); //definir a cor de fundo - preto
    glClear(GL_COLOR_BUFFER_BIT); //limpar o buffer de cor

    glMatrixMode(GL_PROJECTION); //seleção da matriz de projeção
    glLoadIdentity(); //carregar a matriz identidade

    //projeção em duas dimensões
    gluOrtho2D(-20.0f, 20.0f, -20.0f, 20.0f); //definição da tela = mundo de desenho de -20 a 20

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    //oTriangulo->desenho();//oTriangulo é um apontador, entao para invocar um metodo sobre um objeto usar ->
    //oTriangulo2->desenho();
    oQuadrado3->desenho();
    oTriangulo3->desenho();
    oQuadrado->desenho();
    oQuadrado1->desenho();
    oQuadrado2->desenho();
    ;
    glutSwapBuffers(); //troca de buffer

}

int main (int argc, char** argv)
{

    //Bloco de configuraçao inicial da freeGLUT e da janela de visualização
    glutInit(&argc, argv); //inicializaçao da freeGLUT

    glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE); //Inicialização do modo de renderização
    //GLUT_DOUBLE duplo buffer, desenhar sobre um buffer que nao esta a ser usado para a renderizaçao,
    //estamos a fazer off screen randering, logo temos de fazer a troca de buffer

    glutInitWindowSize(1000,1000); //Definir o tamanho da janela 1000x1000

    glutInitWindowPosition(0,0); //Definir a posição inicial da janela

    glutCreateWindow ("Figuras Geometricas"); //Criar a janela

    //Registo das callbacks

    //callback desenho
    glutDisplayFunc(desenho);

    //callback de teclado
    glutKeyboardFunc(teclado);

    //Iniciar o cliclo de eventos
    glutMainLoop();

    //std::cout << "Hello World!\n";


    return 0;
}

